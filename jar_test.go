package sniffa

import (
	"reflect"
	"testing"
)

func TestRootPackages(t *testing.T) {
	filename := "testdata/junit-4.12.jar"
	want := []string{
		"junit.extensions",
		"junit.framework",
		"junit.runner",
		"junit.textui",
		"org.junit",
	}
	ps, err := Packages(filename)
	if err != nil {
		t.Fatal(err)
	}
	got := RootPackages(ps)
	if !reflect.DeepEqual(want, got) {
		t.Fatalf("want %+v but got %+v", want, got)
	}
}
