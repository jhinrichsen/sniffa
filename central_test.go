package sniffa

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"testing"
)

const (
	checksum = "2973d150c0dc1fefe998f834810d68f278ea58ec"
)

func TestCentralOnline(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping online request in short mode")
	}
	log.Println("running online")
	cr, err := CentralSearchSha1(checksum)
	if err != nil {
		t.Fatal(err)
	}
	if cr.ResponseHeader.Status != 0 {
		t.Fatalf("want status 0 but got %d", cr.ResponseHeader.Status)
	}
	if cr.Response.NumFound == 0 {
		t.Fatalf("want some result but got 0")
	}
}

func TestCentralOffline(t *testing.T) {
	const (
		filename = "testdata/central-checksum-result.json"
	)
	buf, err := ioutil.ReadFile(filename)
	if err != nil {
		t.Fatal(err)
	}
	var cr CentralResponse
	if err := json.Unmarshal(buf, &cr); err != nil {
		t.Fatal(err)
	}
	if cr.ResponseHeader.Status != 0 {
		t.Fatalf("want status 0 but got %d", cr.ResponseHeader.Status)
	}
	if cr.Response.NumFound != 1 {
		t.Fatalf("want 1 result but got %d", cr.Response.NumFound)
	}
}
