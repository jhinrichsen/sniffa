package sniffa

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

const (
	central           = "https://search.maven.org/"
	searchChecksumUrl = "solrsearch/select?q=1:%s&wt=json"
)

// Cannot get > to work, using embedded structs instead
type CentralResponse struct {
	ResponseHeader struct {
		Status int `json:"status"`
	} `json:"responseHeader"`
	Response struct {
		NumFound int          `json:"numFound"`
		Docs     []CentralDoc `json:"docs"`
	} `json:"response"`
}

type CentralDoc struct {
	ConciseNotation string   `json:"id"`
	Timestamp       int64    `json:"timestamp"`
	Tags            []string `json:"tags"`
}

func CentralSearchSha1(checksum string) (CentralResponse, error) {
	var cr CentralResponse
	res, err := http.Get(central + fmt.Sprintf(searchChecksumUrl, checksum))
	if err != nil {
		return cr, err
	}
	buf, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return cr, err
	}
	if err := json.Unmarshal(buf, &cr); err != nil {
		return cr, err
	}
	return cr, nil
}
