package sniffa

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

const (
	ConfUserPath = ".jfrog"
	ConfFilename = "jfrog-cli.conf"
)

// Exists returns true if filename exists.
func Exists(filename string) (bool, error) {
	_, err := os.Stat(filename)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	var mu bool
	return mu, err
}

// Convenience function for UserConfigPath() and Exists()
func HasUserConfig() (bool, error) {
	p, err := UserConfigPath()
	if err != nil {
		var mu bool
		return mu, err
	}
	return Exists(p)
}

func ReadConfig(filename string) (ArtifactoryConfig, error) {
	var ai ArtifactoryConfig
	buf, err := ioutil.ReadFile(filename)
	if err != nil {
		return ai, err
	}
	err = json.Unmarshal(buf, &ai)
	return ai, err
}

func ReadUserConfig() (ArtifactoryConfig, error) {
	var ai ArtifactoryConfig
	p, err := UserConfigPath()
	if err != nil {
		return ai, err
	}
	return ReadConfig(p)
}

// UserConfigPath returns default location for Jfrog CLI user config
// (~/.jfrog/jfrog-cli.conf).
func UserConfigPath() (string, error) {
	d, err := os.UserHomeDir()
	if err != nil {
		return "", err
	}
	return filepath.Join(d, ConfUserPath, ConfFilename), nil
}

// Artifactory is a Version 1 JSON based Jfrog CLI configuration.
type ArtifactoryConfig struct {
	Instances []ArtifactoryInstance `json:"artifactory"`
	Version   string                `json:"Version"`
}

func (a ArtifactoryConfig) Locate(l Locater, notFound error) (ArtifactoryInstance, error) {
	for _, ai := range a.Instances {
		if l(ai) {
			return ai, nil
		}
	}
	return ArtifactoryInstance{}, notFound
}

func (a ArtifactoryConfig) FindServerID(ID string) (ArtifactoryInstance, error) {
	for _, ai := range a.Instances {
		if ai.IsDefault {
			return ai, nil
		}
	}
	return a.Instances[0], fmt.Errorf("instance %q not found", ID)
}

func (a ArtifactoryConfig) DefaultInstance() (ArtifactoryInstance, error) {
	notFound := fmt.Errorf("cannot find default instance")
	f := func(ai ArtifactoryInstance) bool {
		if ai.IsDefault {
			return true
		}
		return false
	}
	return a.Locate(f, notFound)
}
