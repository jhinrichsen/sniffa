package sniffa

type ChecksumType int

const (
	Unknown ChecksumType = iota
	Md5
	Sha1
	Sha256
)

func (a ChecksumType) String() string {
	switch a {
	case Md5:
		return "md5"
	case Sha1:
		return "sha1"
	case Sha256:
		return "sha256"
	default:
		return "undefined"
	}
}

func Sniff(checksum string) ChecksumType {
	switch len(checksum) {
	case 32:
		return Md5
	case 40:
		return Sha1
	case 64:
		return Sha256
	default:
		return Unknown
	}
}
