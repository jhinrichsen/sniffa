package sniffa

import (
	"encoding/json"
	"io/ioutil"
	"testing"
)

func TestDefaultInstance(t *testing.T) {
	cfg, err := ReadConfig(configFilename)
	if err != nil {
		t.Fatal(err)
	}

	want := ArtifactoryInstance{
		Url:       "http://repo.jfrog.org/artifactory/",
		User:      "anonymous",
		Password:  "anonymous",
		ServerID:  "jfrog-repo",
		IsDefault: true}
	got, err := cfg.DefaultInstance()
	if want != got {
		t.Fatalf("want %+v but got %+v", want, got)
	}
}

func TestSearchResult(t *testing.T) {
	buf, err := ioutil.ReadFile("testdata/artifactory-checksum-result.json")
	if err != nil {
		t.Fatal(err)
	}
	var srs SearchResults
	if err := json.Unmarshal(buf, &srs); err != nil {
		t.Fatal(err)
	}
	wantLen := 2
	gotLen := len(srs.Results)
	if wantLen != gotLen {
		t.Fatalf("want %v but got %v", wantLen, gotLen)
	}
	wantPath := "/junit/junit/4.12/junit-4.12.jar"
	gotPath := srs.Results[0].Path
	if wantPath != gotPath {
		t.Fatalf("want %q but got %q", wantPath, gotPath)
	}
}
