package main

// return codes:
// 1: general error
// 2: no existing Jfrog CLI configuration
// 3: bad usage
import (
	"crypto/md5"
	"crypto/sha1"
	"encoding/json"
	"flag"
	"fmt"
	"hash"
	"io"
	"log"
	"os"
	"path/filepath"
	"strconv"

	"gitlab.com/jhinrichsen/gav"
	"gitlab.com/jhinrichsen/sniffa"
)

// initialized externally
var Version string

type Report []ReportElement
type ReportElement struct {
	Ref          string
	Filename     string
	Md5          string
	Sha1         string
	Paths        []string // path into one or more repositories
	Gavs         []string // concise notation
	JavaPackages []string
}

func die(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func probablyHasGav(srs sniffa.SearchResults) bool {
	for _, sr := range srs.Results {
		if sr.MimeType == "application/java-archive" {
			return true
		}
	}
	return false
}

func abortOnMissingConfig() {
	b, err := sniffa.HasUserConfig()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v", err)
		os.Exit(1)
	}
	if !b {
		fmt.Fprintf(os.Stderr,
			"cannot find Jfrog CLI, make sure to run 'jfrog rt config'")
		os.Exit(2)
	}
}

// Handle commandline parameter, exit if bad usage, return jfrog cli server ID
func cmdline() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [<checksum> | <file>]*\n", os.Args[0])
		flag.PrintDefaults()
	}
	flag.Parse()
	if len(flag.Args()) == 0 {
		flag.Usage()
		os.Exit(3)
	}
	return flag.Args()
}

func md5Checksum(filename string) (string, error) {
	return checksum(filename, md5.New())
}

func sha1Checksum(filename string) (string, error) {
	return checksum(filename, sha1.New())
}

// Map a file or a checksum to a checksum
func checksum(filename string, hasher hash.Hash) (string, error) {
	f, err := os.Open(filename)
	if err != nil {
		return filename, err
	}
	defer f.Close()
	if _, err := io.Copy(hasher, f); err != nil {
		return "", err
	}
	// use hex representation
	return fmt.Sprintf("%x", hasher.Sum(nil)), nil
}

func distinctPaths(srs sniffa.SearchResults) []string {
	paths := make(map[string]bool)
	for _, sr := range srs.Results {
		paths[sr.Path] = true
	}
	var ss []string
	for e := range paths {
		ss = append(ss, e)
	}
	return ss
}

func initialize(args []string) (Report, error) {
	var r Report
	for i, arg := range cmdline() {
		var re ReportElement
		re.Ref = strconv.Itoa(i)
		re.Filename = filepath.Clean(arg)
		c5, err := md5Checksum(arg)
		if err != nil {
			return r, err
		}
		re.Md5 = c5

		c1, err := sha1Checksum(arg)
		if err != nil {
			return r, err
		}
		re.Sha1 = c1
		r = append(r, re)
	}
	return r, nil
}

func resolve(report *Report, client sniffa.ArtifactoryClient) error {
	for i := range *report {
		// resolve against Central
		crs, err := sniffa.CentralSearchSha1((*report)[i].Sha1)
		if err != nil {
			log.Printf(err.Error())
			continue
		}
		for j := range crs.Response.Docs {
			log.Printf("adding central GAV %q\n",
				crs.Response.Docs[j].ConciseNotation)
			(*report)[i].Gavs = append((*report)[i].Paths,
				crs.Response.Docs[j].ConciseNotation)
		}
		if len((*report)[i].Gavs) > 0 {
			continue
		}

		// nothing found, resolve against artifactory
		srs, err := client.Search((*report)[i].Md5)
		if err != nil {
			log.Printf(err.Error())
			continue
		}
		if len(srs.Results) == 0 {
			(*report)[i].Paths = []string{"not found"}

			// if artifact cannot be resolved, show Java packages
			ps, err := sniffa.Packages((*report)[i].Filename)
			if err != nil {
				ss := []string{err.Error()}
				(*report)[i].JavaPackages = ss
				continue
			}
			ps = sniffa.RootPackages(ps)
			(*report)[i].JavaPackages = ps
			continue
		}
		(*report)[i].Paths = distinctPaths(srs)

		if !probablyHasGav(srs) {
			continue
		}
		for j := range (*report)[i].Paths {
			coord := gav.DefaultLayout((*report)[i].Paths[j])
			s := coord.ConciseNotation()
			(*report)[i].Gavs = append((*report)[i].Gavs, s)
		}
	}
	return nil
}

func main() {
	abortOnMissingConfig()
	r, err := initialize(cmdline())
	die(err)
	client, err := sniffa.DefaultClient()
	die(err)
	if err := resolve(&r, client); err != nil {
		die(err)
	}

	buf, err := json.MarshalIndent(r, "", "  ")
	die(err)
	fmt.Println(string(buf))
}
