package sniffa

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func DefaultClient() (ArtifactoryClient, error) {
	var c ArtifactoryClient
	ac, err := ReadUserConfig()
	if err != nil {
		return c, err
	}
	ai, err := ac.DefaultInstance()
	if err != nil {
		return c, err
	}
	return ArtifactoryClient{ai, http.Client{}}, nil
}

type Locater func(ai ArtifactoryInstance) bool

type ArtifactoryInstance struct {
	Url       string `json:"url"`
	User      string `json:"user"`
	Password  string `json:"password"`
	ServerID  string `json:"serverId"`
	IsDefault bool   `json:"isDefault"`
}

type ArtifactoryClient struct {
	ArtifactoryInstance
	Client http.Client
}

type SearchResults struct {
	Results []SearchResult `json:"results"`
}

type SearchResult struct {
	Repo     string `json:"repo"`
	Path     string `json:"path"`
	MimeType string `json:"mimeType"`
}

// Join will join path parts of an URI and make sure only single slashes "/" are
// used.
// strings.Join() will always add slashes, filepath.Join() cannot cope with the
// protocol (http:///server.com)
func Join(path string, relpaths ...string) string {
	const slash = "/"
	path = strings.TrimRight(path, slash)
	for i := range relpaths {
		relpaths[i] = strings.TrimLeft(relpaths[i], slash)
		relpaths[i] = strings.TrimRight(relpaths[i], slash)
	}
	return strings.Join(append([]string{path}, relpaths...), slash)
}

// SearchChecksumUrl returns a fully initialized URL depending on the type of
// checksum provided.
func (a ArtifactoryClient) SearchChecksumUrl(checksum string) string {
	// Be very cautios about double slashes, they lead to http status code
	// 500: "Repo key cannot be empty. Path: api/search/checksum"
	uri := Join(a.Url, "api/search/checksum")
	t := Sniff(checksum)
	u := fmt.Sprintf("%s?%s=%s", uri, t, checksum)
	return u
}

func ok(rc int) bool {
	return rc/100 == 2
}

func probablyHasGav(s SearchResult) bool {
	return s.MimeType == "application/java-archive"
}

func (a ArtifactoryClient) searchChecksum(s string) ([]byte, error) {
	u := a.SearchChecksumUrl(s)
	req, err := http.NewRequest("GET", u, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("X-Result-Detail", "info")
	res, err := a.Client.Do(req)
	if err != nil {
		return nil, err
	}
	if !ok(res.StatusCode) {
		return nil, fmt.Errorf("search request returns status code %d", res.StatusCode)
	}

	defer res.Body.Close()
	return ioutil.ReadAll(res.Body)
}

// Artifactory does not find remote checksums, i.e. artifacts
// that have not yet been used are invisible
func (a ArtifactoryClient) Search(checksum string) (SearchResults, error) {
	var srs SearchResults
	buf, err := a.searchChecksum(checksum)
	if err != nil {
		return srs, err
	}
	if err := json.Unmarshal(buf, &srs); err != nil {
		return srs, err
	}
	return srs, nil
}
