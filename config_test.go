package sniffa

import (
	"os"
	"path/filepath"
	"testing"
)

const configFilename = "testdata/jfrog-cli.conf"

func TestExists(t *testing.T) {
	want := true
	got, err := Exists(configFilename)
	if err != nil {
		t.Fatal(err)
	}
	if want != got {
		t.Fatalf("want %v but got %v", want, got)
	}
}

func TestNotExists(t *testing.T) {
	want := false
	got, err := Exists(filepath.Join(os.TempDir(), configFilename))
	if err != nil {
		t.Fatal(err)
	}
	if want != got {
		t.Fatalf("want %v but got %v", want, got)
	}
}

func TestReadConfig(t *testing.T) {
	_, err := ReadConfig(configFilename)
	if err != nil {
		t.Fatal(err)
	}
}
