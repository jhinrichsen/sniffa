package sniffa

import (
	"archive/zip"
	"path/filepath"
	"sort"
	"strings"
)

func isClass(filename string) bool {
	return strings.HasSuffix(filename, ".class")
}

// Package returns Java package of given class in dot notation
func Package(class string) string {
	return strings.ReplaceAll(filepath.Dir(class), "/", ".")
}

// Packages returns all Java packages in a jar (in dot '.' notation)
// zip files require random access, so cannot be streamed
func Packages(filename string) ([]string, error) {
	var ss []string
	zr, err := zip.OpenReader(filename)
	if err != nil {
		return ss, err
	}
	defer zr.Close()

	for _, f := range zr.File {
		if isClass(f.Name) {
			ss = append(ss, Package(f.Name))
		}
	}

	return ss, nil
}

// RootPackages returns distinct parent packages (in dot '.' notation)
func RootPackages(packages []string) []string {
	if len(packages) == 0 {
		return packages
	}
	sort.Strings(packages)
	var ss []string
	ss = append(ss, packages[0])
	j := 0
	for i := range packages[1:] {
		if strings.HasPrefix(packages[i], ss[j]) {
			continue
		}
		// new root package
		ss = append(ss, packages[i])
		j++
	}
	return ss
}
